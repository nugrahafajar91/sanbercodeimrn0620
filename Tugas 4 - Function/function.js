// SOAL NO 1
console.log('=== SOAL NO.1 ===')
function teriak() {
    return "Halo Sanbers!"
}
console.log(teriak())

// SOAL NO 2
console.log('\n=== SOAL NO.2 ===')
function kalikan(num1, num2) {
    var hasil = num1 * num2
    return hasil
}

var num1 = 12
var num2 = 4
 
var hasilKali = kalikan(num1, num2)
console.log(hasilKali) // 48

// SOAL NO 3
console.log('\n=== SOAL NO.3 ===')
function introduce(name, age, address, hobby) {
    var hasil = "Nama saya " + name + ", umur saya " + age + ", alamat saya di " + address + ", dan saya punya hobby yaitu " + hobby + "!"
    return hasil
}

var name = "Agus"
var age = 30
var address = "Jln. Malioboro, Yogyakarta"
var hobby = "Gaming"
 
var perkenalan = introduce(name, age, address, hobby)
console.log(perkenalan) // Menampilkan "Nama saya Agus, umur saya 30 tahun, alamat saya di Jln. Malioboro, Yogyakarta, dan saya punya hobby yaitu Gaming!" 