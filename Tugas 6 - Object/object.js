// SOAL NO 1
console.log('=== SOAL NO.1 ===')
function arrayToObject(arr) {
    // Code di sini 
    var arrLength = arr.length 
    var result = {}
    var now = new Date()
    var thisYear = now.getFullYear() // 2020 (tahun sekarang)
    for(var i = 0; i < arrLength; i++){
        var key = i+1 + '. ' + arr[i][0] + ' ' + arr[i][1] + ': '
        var tahun = arr[i][3]
        var age = (tahun > 0 && tahun <= thisYear) ? thisYear - tahun : "Invalid Birth Year"
        var biodata = {
            'firstName': arr[i][0],
            'lastName': arr[i][1],
            'gender': arr[i][2],
            'age': age
        }
        result[key] = biodata
    }
    console.log(result)
}
 
// Driver Code
var people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ]
arrayToObject(people) 
/*
    1. Bruce Banner: { 
        firstName: "Bruce",
        lastName: "Banner",
        gender: "male",
        age: 45
    }
    2. Natasha Romanoff: { 
        firstName: "Natasha",
        lastName: "Romanoff",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/
 
var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ]
arrayToObject(people2) 
/*
    1. Tony Stark: { 
        firstName: "Tony",
        lastName: "Stark",
        gender: "male",
        age: 40
    }
    2. Pepper Pots: { 
        firstName: "Pepper",
        lastName: "Pots",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/
 
// Error case 
arrayToObject([]) // ""

// SOAL NO 2
console.log('\n=== SOAL NO.2 ===')
function shoppingTime(memberId, money) {
    // you can only write your code here!
    var listProducts = [
        ['Sepatu Brand Stacattu', 1500000],
        ['Baju brand Zoro', 500000],
        ['Baju brand H&N', 250000],
        ['Sweater brand Uniklooh', 175000],
        ['Casing Handphone', 50000]
    ]
    var purchase = []
    var changeMoney = money
    var result
    
    if(memberId === undefined || memberId === '') {
        result = 'Mohon maaf, toko X hanya berlaku untuk member saja'
    } else if(money < 50000) {
        result = 'Mohon maaf, uang tidak cukup'
    } else {
        for (let i in listProducts) {
            if (changeMoney >= listProducts[i][1]) {
                changeMoney -= listProducts[i][1];     
                purchase.push(listProducts[i]);
            } 
        }

        result = {
            memberId: memberId,
            money: money,
            listPurchased: purchase,
            changeMoney: changeMoney
        }
    }
    return result
}
   
// TEST CASES
console.log(shoppingTime('1820RzKrnWn08', 2475000));
console.log(shoppingTime('82Ku8Ma742', 170000));
console.log(shoppingTime('', 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
console.log(shoppingTime('234JdhweRxa53', 15000)); //Mohon maaf, uang tidak cukup
console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja

// SOAL NO 3
console.log('\n=== SOAL NO.3 ===')
function naikAngkot(arrPenumpang) {
    rute = ['A', 'B', 'C', 'D', 'E', 'F'];
    var result = []
    var totalRute = 0; 
    //your code here
    for(let i in arrPenumpang){
        indexDari = rute.indexOf(arrPenumpang[i][1])
        indexTujuan = rute.indexOf(arrPenumpang[i][2])
        totalRute = (indexTujuan - indexDari) * 2000

        var penumpang = {
            penumpang: arrPenumpang[i][0],
            naikDari: arrPenumpang[i][1],
            tujuan: arrPenumpang[i][2],
            bayar: totalRute
        }
        result.push(penumpang)
    }
    return result
}
   
//TEST CASE
console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
// [ { penumpang: 'Dimitri', naikDari: 'B', tujuan: 'F', bayar: 8000 },
//   { penumpang: 'Icha', naikDari: 'A', tujuan: 'B', bayar: 2000 } ]

console.log(naikAngkot([])); //[]