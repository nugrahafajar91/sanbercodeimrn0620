// SOAL NO 1
console.log('=== SOAL NO.1 ===')

class Score {
    // Code class di sini
    constructor(points, email, subject){
        this.subject = subject
        this.points = points
        this.email = email
    }
    average(){
        var sum = 0;
        if(Array.isArray(this.points)){
            for(var i = 0; i < this.points.length; i++) {
                sum += this.points[i];
            }
            sum = sum / this.points.length;
        }else{
            sum = this.points
        }
        console.log(`Subject: ${this.subject} \n Points: ${sum} \n Email: ${this.email}`)
        
    }
}
// Single Points
console.log('-- Single Points --')
var score = new Score(2, 'fnugraha11@gmail.com', 'Quiz-1');
score.average()

// Array Points
console.log('\n-- Array Points --')
var score = new Score([3, 4, 7, 8], 'fnugraha11@gmail.com', 'Quiz-1');
score.average()


// SOAL NO 2
console.log('=== SOAL NO.2 ===')
const data = [
    ["email", "quiz - 1", "quiz - 2", "quiz - 3"],
    ["abduh@mail.com", 78, 89, 90],
    ["khairun@mail.com", 95, 85, 88],
    ["bondra@mail.com", 70, 75, 78],
    ["regi@mail.com", 91, 89, 93]
  ]
  
function viewScores(data, subject) {
    // code kamu di sini
    for(var i = 0; i < data[0].length; i++){
        console.log(data[0][i])
        if(data[0][i] === subject){
            
        }
    }

}
viewScores(data, 'quiz - 1')