// SOAL 1
console.log('=== SOAL IF ELSE ===')
var nama = "Fajar"
var peran = ""

if (nama === '' && peran === ''){
    console.log('Nama harus diisi !!!')
} else if (nama == 'Fajar' && peran == ''){
    console.log('Halo ' + nama + ', Pilih peranmu untuk memulai Game !')
} else if (nama == 'Jay' && peran == 'Penyihir'){
    console.log('Selamat datang di Dunia Werewolf, ' + nama)
    console.log('Halo ' + peran + ' ' + nama + ', kamu dapat melihat siapa yang menjadi werewolf!')
} else if (nama == 'Jey' && peran == 'Guard'){
    console.log('Selamat datang di Dunia Werewolf, ' + nama)
    console.log('Halo ' + peran + ' ' + nama + ', kamu akan membantu melindungi temanmu dari serangan werewolf.')
} else if (nama == 'Joy' && peran == 'Werewolf') {
    console.log('Selamat datang di Dunia Werewolf, ' + nama)
    console.log('Halo ' + peran + ' ' + nama + ', Kamu akan memakan mangsa setiap malam!')
} else {
    console.log('Nothing Condition !')
}

// SOAL 2
console.log('\n=== SOAL SWITCH CASE ===')
var hari = 11; 
var bulan = 2; 
var tahun = 1991;

switch(bulan) {
    case 1:
        bulan = 'Januari'
        break;
    case 2:
        bulan = 'Februari'
        break;
    case 3:
        bulan = 'Maret'
        break;
    case 4:
        bulan = 'April'
        break;
    case 5:
        bulan = 'Mei'
        break;
    case 6:
        bulan = 'Juni'
        break;
    case 7:
        bulan = 'Juli'
        break;
    case 8:
        bulan = 'Agustus'
        break;
    case 9:
        bulan = 'September'
        break;
    case 10:
        bulan = 'Oktober'
        break;
    case 11:
        bulan = 'November'
        break;
    case 12:
        bulan = 'Desember'
        break;
}

switch(true) {
    case hari < 1 || hari > 31:
        hari = 'none'
        break;
}
switch(true) {
    case bulan < 1 || bulan > 12:
        bulan = 'none'
        break;
}
switch(true) {
    case tahun < 1900 || bulan > 2200:
        tahun = 'none'
        break;
}

console.log(hari + ' ' + bulan + ' ' + tahun)