// SOAL NO 1
console.log('=== LOOPING PERTAMA ===')

var a = 2;
while(a <= 20)
{
    if(a % 2 == 0)
    {
        console.log(a + ' - I Love Coding');
    }
    a++;
}

console.log('\n=== LOOPING KEDUA ===')

var b = 20;
while(b >= 2)
{
    if(b % 2 == 0)
    {
        console.log(b + ' - I will become a mobile developer');
    }
    b--; 
}

/// SOAL NO 2
console.log('\n=== LOOPING FOR ===')
for (var c = 1; c <=20; c++){
    if(c % 2 == 0){
        console.log(c + ' - Berkualitas')
    } else if( ((c % 2) != 0) && ((c % 3) == 0) ){
        console.log(c + ' - I Love Coding')
    }else {
        console.log(c + ' - Santai')
    }
}

// SOAL NO 3
console.log('\n=== LOOPING PERSEGI PANJANG ===')
var columns = 8;
var rows = 4;
var outputText = ''
for (i = 0; i < rows; i++)
{
    for (j = 0; j < columns; j++) 
    { 
        outputText = outputText + "#"
    }
    outputText = outputText + "\n"
} 
console.log(outputText)

// SOAL NO 4
console.log('\n=== LOOPING TANGGA ===')
var tangga='';
for (i=1; i<=7; i++)
{   
    for (j=1; j<=i; j++)
    {
        tangga = tangga + '#';   
    }

    console.log(tangga);
    tangga = ''
}

// SOAL NO 5
console.log('\n=== LOOPING PAPAN CATUR ===')
var col = 8;
var row = 8;
var outputCatur = ''
for (k = 1; k <= row; k++)
{
    for (l = 1; l <= col; l++) 
    { 
        if (k % 2 != 0 && l % 2 == 0){
            outputCatur = outputCatur + " #"
        }else if (k % 2 == 0 && l % 2 != 0){
            outputCatur = outputCatur + "# "
        }
    }
    outputCatur = outputCatur + "\n"
} 
console.log(outputCatur)