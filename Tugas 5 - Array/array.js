// SOAL NO 1
console.log('=== SOAL NO.1 ===')
function range(startNum, finishNum) {
    var resultArray = []
    if(startNum === undefined || finishNum === undefined) {
        return -1
    } else if(startNum < finishNum) {
        for (var i = startNum; i <= finishNum; i++){
            resultArray.push(i)
        }
    } else if(startNum > finishNum) {
        for (var i = startNum; i >= finishNum; i--){
            resultArray.push(i)
        }
    }
    return resultArray
}
console.log(range(1, 10)) //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(1)) // -1
console.log(range(11,18)) // [11, 12, 13, 14, 15, 16, 17, 18]
console.log(range(54, 50)) // [54, 53, 52, 51, 50]
console.log(range()) // -1 

// SOAL NO 2
console.log('\n=== SOAL NO.2 ===')
function rangeWithStep(startNum, finishNum, step) {
    var resultArray = []
    if(startNum < finishNum) {
        for (var i = startNum; i <= finishNum; i+=step){
            resultArray.push(i)
        }
    } else if(startNum > finishNum) {
        for (var i = startNum; i >= finishNum; i-=step){
            resultArray.push(i)
        }
    }
    return resultArray
}
console.log(rangeWithStep(1, 10, 2)) // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)) // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)) // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)) // [29, 25, 21, 17, 13, 9, 5] 

// SOAL NO 3
console.log('\n=== SOAL NO.3 ===')
function sum(startNum, finishNum, step) {
    // console.log(startNum +'-'+ finishNum +'-'+ step)
    var result = 0;
    var numArray = [1]
    if(startNum !== undefined && finishNum === undefined && step === undefined) {
        return 1
    }else if(step === undefined) {
        numArray = range(startNum, finishNum)
    }else{
        numArray = rangeWithStep(startNum || 0, finishNum || 0, step)   
    }
    

    for (var i = 0; i < numArray.length; i++){
        result += numArray[i]
    }
    return result
}
console.log(sum(1,10)) // 55
console.log(sum(5, 50, 2)) // 621
console.log(sum(15,10)) // 75
console.log(sum(20, 10, 2)) // 90
console.log(sum(1)) // 1
console.log(sum()) // 0 

// SOAL NO 4
console.log('\n=== SOAL NO.4 ===')
var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
] 

function dataHandling(input) {
    var result = ''
    for (var i = 0; i < input.length; i++){
        var id = 'Nomor ID: '+ input[i][0] + '\n'
        var nama = 'Nama: '+ input[i][1] + '\n'
        var ttl = 'TTL: '+ input[i][2] + ', ' + input[i][3] + '\n'
        var hobi = 'Hobi: '+ input[i][4] + '\n\n'

        result += id + nama + ttl + hobi
    }
    return result
}

console.log(dataHandling(input))

// SOAL NO 5
console.log('=== SOAL NO.5 ===')
function balikKata(str) {
    var currentString = str;
    var newString = '';
    for (let i = str.length - 1; i >= 0; i--) {
        newString = newString + currentString[i];
    }

    return newString;
}

console.log(balikKata("Kasur Rusak")) // kasuR rusaK
console.log(balikKata("SanberCode")) // edoCrebnaS
console.log(balikKata("Haji Ijah")) // hajI ijaH
console.log(balikKata("racecar")) // racecar
console.log(balikKata("I am Sanbers")) // srebnaS ma I 

// SOAL NO 6
console.log('\n=== SOAL NO.6 ===')
var input = ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"];
function dataHandling2(input) {
    input.splice(1, 1, 'Roman Alamsyah Elsharawy')
    input.splice(2, 1, 'Provinsi Bandar Lampung')
    input.splice(4, 1)
    input.splice(4, 1, 'Pria', 'SMA Internasional Metro')
    var newInformation = input
    var ttl = newInformation[3]
    var month = ttl.split('/').map(Number)
    var month1 = ttl.split('/')
    var name = newInformation[1]

    console.log(newInformation)
    console.log(monthValue(month[1]))
    month.sort(function (value1, value2) { return value1 < value2 } ) ;
    console.log(month) 
    console.log(month1.join('-'))
    console.log(name.slice(0, 15))
    
}

function monthValue(bulan) {
    var month = ''
    switch(parseInt(bulan)) {
        case 1:
            month = 'Januari'
            break;
        case 2:
            month = 'Februari'
            break;
        case 3:
            month = 'Maret'
            break;
        case 4:
            month = 'April'
            break;
        case 5:
            month = 'Mei'
            break;
        case 6:
            bulan = 'Juni'
            break;
        case 7:
            month = 'Juli'
            break;
        case 8:
            month = 'Agustus'
            break;
        case 9:
            month = 'September'
            break;
        case 10:
            bulan = 'Oktober'
            break;
        case 11:
            month = 'November'
            break;
        case 12:
            month = 'Desember'
            break;
    }
    return month
}

dataHandling2(input)